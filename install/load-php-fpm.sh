PHP_FPM_BIN="php-fpm"
PHP_FPM_PID="/var/run/php-fpm.pid"

mstart() {
	echo -n "Starting php_fpm "
	$PHP_FPM_BIN
}

mstop() {
	if [ ! -r $PHP_FPM_PID ] ; then
		echo -n "php-fpm not run at a PID "
		return
	fi
	local pid=`head $PHP_FPM_PID`

	if [[ $1 == 'quit' ]]; then
		echo -n "Shutting down php_fpm gracefully "
		kill -QUIT `cat $PHP_FPM_PID`
	else
		echo -n "Shutting down php_fpm "
		kill -TERM `cat $PHP_FPM_PID`
	fi
}


case "$1" in
	start)
		mstop
		mstart
		;;

	stop)
		mstop
		;;

	quit)
		mstop quit
		;;

	restart)
		mstop
		mstart
		;;

	reload)
		echo -n "Reload service php-fpm "

		if [ ! -r $PHP_FPM_PID ] ; then
			echo "warning, no pid file found - php-fpm is not running ?"
			exit 1
		fi

		kill -USR2 `cat $PHP_FPM_PID`

		echo " done"
		;;

	*)
		echo "Usage: {start|stop|quit|restart|reload}"
		exit 1
		;;
esac
