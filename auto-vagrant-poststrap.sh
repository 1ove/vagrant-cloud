#!/usr/bin/env bash

REMOTE_HOST="http://www.morysky.win"
VIM_REMOTE_PATH=$REMOTE_HOST"/static/git_sync/cloud/general/vim/"

mkdir -p /tmp/vim
cd /tmp/vim

wget $VIM_REMOTE_PATH/"install-core.sh"
bash install-core.sh
wget $VIM_REMOTE_PATH/"install-huge.sh"
bash install-huge.sh

cd ~/.vim/bundle/YouCompleteMe/
cp /vagrant/install/vim/CMakeLists.txt third_party/ycmd/cpp/ycm/CMakeLists.txt
if test -f "/vagrant/clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-14.04.tar.xz" ;then
    python install.py  --clang-completer
    sed -i "s/let g:neocomplete#enable_at_startup=1/let g:neocomplete#enable_at_startup=0/g" ~/.vimrc
    sed -i "s/g:youcompleteme_switch=0/g:youcompleteme_switch=1/g" ~/.vimrc
fi

cd /tmp
rm -rf /tmp/vim

mkdir -p /vagrant/runtime/www
mkdir -p /vagrant/runtime/var/log
