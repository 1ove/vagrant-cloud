# Usage

```
vagrant up
vagrant ssh
```

## Port

4567 -> 80

## Mysql

user : root
password : qwe123

## Nginx

```
document root : /vagrant/runtime/www/laravel
```

# Important

You must put llvm tar into current dir(Same with this Vagrantfile), so vim's YouCompleteMe plugin could find it, Or you can only manually install.

```
wget http://releases.llvm.org/3.9.0/clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-14.04.tar.xz
mv clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-14.04.tar.xz /vagrant/

cd ~/.vim/bundle/YouCompleteMe/
python install.py  --clang-completer
```
