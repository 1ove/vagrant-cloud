#!/usr/bin/env bash

# install composer
# maybe different
COMPOSER_SOURCE="http://www.morysky.win/static/git_sync/cloud_huge/composer.phar"
COMPOSER_PREFIX="/home/work"

COMPOSER_BIN_PREFIX=$COMPOSER_PREFIX"/bin"
COMPOSER_BIN=$COMPOSER_BIN_PREFIX"/composer"
COMPOSER_VENDOR_BIN_PREFIX=".config/composer/vendor/bin"

LARAVEL_BIN="$COMPOSER_PREFIX/$COMPOSER_VENDOR_BIN_PREFIX/laravel"

which composer
COMPOSER_NOT_EXIST=$?
if [ $COMPOSER_NOT_EXIST -eq 1 ];
then
    if test -f $COMPOSER_BIN ;
    then
        echo "Composer bin has already EXISTS";
    else
        wget $COMPOSER_SOURCE
        mkdir -p $COMPOSER_BIN_PREFIX
        mv composer.phar $COMPOSER_BIN
    fi
    echo "export PATH=\$PATH:$COMPOSER_BIN_PREFIX:$COMPOSER_PREFIX/$COMPOSER_VENDOR_BIN_PREFIX" >> ~/.bashrc
    chmod u+x $COMPOSER_BIN
fi

$COMPOSER_BIN config -g repo.packagist composer https://packagist.phpcomposer.com

# install laravel
$COMPOSER_BIN global require "laravel/installer"

echo $LARAVEL_BIN

chmod u+x $LARAVEL_BIN

cd /vagrant/runtime/www
$LARAVEL_BIN new laravel
