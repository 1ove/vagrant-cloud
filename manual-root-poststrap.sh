#!/usr/bin/env bash

if test ! -f "/var/swap.1"; then
    dd if=/dev/zero of=/var/swap.1 bs=1M count=2048
    mkswap /var/swap.1
    swapon /var/swap.1
fi

swap_exist=`cat /etc/fstab | grep "/var/swap.1"`
if [[ $swap_exist == "" ]]; then
    echo "/var/swap.1 swap swap defaults 0 0" >> /etc/fstab
fi

# install php
if [ "$1"x = "--with-php"x ]; then
    mkdir -p /tmp/php
    cd /tmp/php
    wget http://hk1.php.net/get/php-5.6.30.tar.bz2/from/this/mirror

    tar xvjf mirror

    cd php-5.6.30

    ./buildconf

    ./configure \
        --prefix=/usr/local \
        --with-curl \
        --with-openssl \
        --with-zlib \
        --enable-fpm \
        --with-readline \
        --enable-mbstring \
        --enable-zip \
        --with-pdo-mysql \
        --disable-fileinfo \
        --with-mcrypt

    make && make install

    mv php.ini-development /usr/local/lib/php.ini
    cp /vagrant/install/php-fpm.conf /usr/local/etc/php-fpm.conf
    cp /vagrant/install/php.ini /usr/local/lib/php.ini

    cd /tmp
    rm -rf /tmp/php
fi

